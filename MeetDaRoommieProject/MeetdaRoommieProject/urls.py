from django.conf.urls import url
from django.contrib import admin
from MeetdaRoommieApp import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings
from django.urls import include,path

from MeetdaRoommieApp.views import SignUpView

from django.contrib.auth import views as auth_views


urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'', views.HomePageView.as_view(), name='home'),
    path('signup/', SignUpView.as_view(), name='signup'),
    path('', include('django.contrib.auth.urls')),
    path(r'edit_profile/', views.EditProfileView.as_view(), name='edit_profile'),

    url(r'about_us', views.about_us, name='about_us'),
    url(r'contact_us', views.contact_us.as_view(), name='contact_us'),
    url(r'my_profile', views.my_profile, name='my_profile'),
    url(r'create_profile', views.create_profile, name='create_profile'),
    #url(r'adjust_search', views.adjust_search, name='adjust_search'),
    url(r'find_roommates', views.find_roommates.as_view(), name='find_roommates'),
    url(r'matches', views.matches, name='matches'),
    url(r'test', views.test, name='test')

]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)




