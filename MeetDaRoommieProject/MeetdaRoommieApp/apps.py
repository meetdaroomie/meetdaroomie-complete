from django.apps import AppConfig


class MeetdaroomieappConfig(AppConfig):
    name = 'MeetdaRoommieApp'
