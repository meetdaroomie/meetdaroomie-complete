from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator

from .models import CustomUser, UserDetails, Match
from django.views import View
from django.views.generic import TemplateView
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from .userForms import CustomUserCreationForm, UserDetailsForm, MatchForm, ContactForm
from django.http import request, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse


def test(request):
    users = CustomUser.objects.all()
    details = UserDetails.objects.all()
    person = request.user
    matches = Match.objects.exclude(user=person)
    return render(request, 'test.html', {'matches': matches})

class HomePageView(TemplateView):
    template_name = 'home.html'

class SignUpView(CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'

@login_required(login_url='login')
def my_profile(request):
    person = request.user
    try:
        comment = UserDetails.objects.get(user=person.id)
    except UserDetails.DoesNotExist:
        comment = None

    if comment == None:
        return render(request, 'my_profile.html', {'person':person})
    else:
        detail = UserDetails.objects.get(user=person.id)
        return render(request, 'my_profile.html', {'person':person, 'detail':detail})

@login_required(login_url='login')
def create_profile(request):
    if request.method == 'POST':
        form = UserDetailsForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.save()
            return HttpResponseRedirect(reverse('my_profile'))
        else:
            return render(request, 'create_profile.html', {'form': form})
    else:
        form = UserDetailsForm()
        return render(request, 'create_profile.html', {'form': form})

class EditProfileView(View):
    def get(self, request):

        person = request.user
        profile = UserDetails.objects.get(user=person.id)

        form = UserDetailsForm(instance = profile)
        return render(request, 'create_profile.html', {'form':form})
    def post(self, request):

        person = request.user
        profile = UserDetails.objects.get(user=person.id)
        form = UserDetailsForm(request.POST, request.FILES, instance=profile)
        if form.is_valid():
            form.save()
            print ('Profile saved ' + str(profile.id))
            return HttpResponseRedirect(reverse('my_profile'))
        else:
            return render(request, 'create_profile.html', {'form': form})


def about_us(request):
    return render(request, 'about_us.html')

def login(request):
    return render(request, 'login.html')


@login_required(login_url='login')
def adjust_search(request):
    return render(request, 'adjust_search.html')





class find_roommates(View):

    def get(self, request):
        roomies=[]
        temp = []
        person = request.user
        try:
            comment = UserDetails.objects.get(user=person.id)
        except UserDetails.DoesNotExist:
            comment = None

        if comment == None:
            return render(request, 'find_roommates.html', {'person':person})
        else:
            me = UserDetails.objects.get(user=person.id)
            saved = Match.objects.filter(user=person)
            candidate = UserDetails.objects.all()

            for roomie in candidate:
                if me != roomie:
                    if int(roomie.zip_code) >= int(me.zip_code)-50 and int(roomie.zip_code) <= int(me.zip_code)+50:
                        roomies.append(roomie)
                        temp.append(roomie)

            for i in temp:
                for k in saved:
                    if i.user.id == int(k.matchUser):
                        roomies.remove(i)

            paginator = Paginator(roomies, 3)
            page_number = request.GET.get('page')
            page_obj = paginator.get_page(page_number)
            roomies = paginator.get_page(page_number)
            form = MatchForm()
            return render(request, 'find_roommates.html', {'page_obj':page_obj, 'roomies': roomies, 'form': form, 'saved': saved})

    def post(self, request):
        form = MatchForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.matchUser = request.POST.get("save")
            instance.save()
            return redirect('find_roommates.html')
        return render(request, 'find_roommates.html')


@login_required(login_url='login')
def matches(request):
    roomies = []
    person = request.user
    saved2 = Match.objects.filter(user=person)
    saved = Match.objects.exclude(user=person)

    for x in saved:
        if int(x.matchUser) == person.id:
            for y in saved2:
                if x.user.id == int(y.matchUser):
                    roomies.append(UserDetails.objects.get(user = x.user.id))

    paginator = Paginator(roomies, 2)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    roomies = paginator.get_page(page_number)

    if not roomies:
        return render(request, 'matches.html')

    return render(request, 'matches.html', {'roomies':roomies, 'page_obj':page_obj})



class contact_us(View):

    def get(self, request):
        form = ContactForm()
        return render(request, 'contact_us.html', {'form': form})

    def post(self, request):
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
        return render(request, 'contact_us.html', {'form': form})
