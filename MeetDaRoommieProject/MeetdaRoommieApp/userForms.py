from django import forms
from .models import CustomUser, UserDetails, Match, Contact
from django.contrib.auth.forms import UserCreationForm, UserChangeForm



class CustomUserCreationForm(UserCreationForm):

    class Meta:
        model = CustomUser
        fields = ('username', 'email')


class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = CustomUser
        fields = UserChangeForm.Meta.fields


class UserDetailsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(UserDetailsForm, self).__init__(*args, **kwargs)

    class Meta:
        GENDER_CHOICES = [
            ('Male', 'Male'),
            ('Female', 'Female')
        ]
        PET_CHOICES = [
            ('Yes', 'Yes'),
            ('No', 'No')
        ]
        SMOKE_CHOICES = [
            ('Yes', 'Yes'),
            ('No', 'No')
        ]
        HOUSING_CHOICES = [
            ('Apartment', 'Apartment'),
            ('Mansion', 'Mansion'),
            ('Condominium', 'Condominium'),
            ('House', 'House'),
            ('All', 'All')
        ]
        model = UserDetails
        exclude = ("user",)
        labels = {
            "first_name": "First Name",
            "last_name": "Last Name",
            "age": "Age",
            "zip_code": "Zip code ",
            "gender": "Gender",
            "pets": "Pets",
            "housing_type": "Housing type",
            "smoke": "Smoker",
            "price": "Monthly Rent ($)",
            "bio": "Bio",
            "pic": "Profile photo"
        }
        widgets = {
            "gender": forms.Select(choices=GENDER_CHOICES),
            "pets": forms.Select(choices=PET_CHOICES),
            "smoke": forms.Select(choices=SMOKE_CHOICES),
            "housing_type": forms.Select(choices=HOUSING_CHOICES),
            "bio": forms.Textarea(attrs={
                'placeholder': 'Explain Yourself/ 100 Characters Max...',
                'rows': '10',
                'cols': '30',
                'maxlength': '100',
            }),
            "price": forms.NumberInput(attrs={"min": "0",
                                              "step": "100"})
        }
        help_texts = {
            "price": "This field only accepts numbers",
        }


class MatchForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(MatchForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Match
        exclude = ("user", "matchUser")
        labels = {
        }


class ContactForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Contact
        exclude = ()
        labels = {
            "name": "Name",
            "email": "Email",
            "msg": "Message"
        }
        widgets = {
            "msg": forms.Textarea(attrs={
                'placeholder': '/ 100 Characters Max...',
                'rows': '10',
                'cols': '30',
                'maxlength': '100',
            })
        }
