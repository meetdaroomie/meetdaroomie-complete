from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

def validate_zip(value):
        if len(value) < 5:
            raise ValidationError(
                _('Minimum Zip code length has to be at least 5 digits'),
                params={'value': value},
            )
