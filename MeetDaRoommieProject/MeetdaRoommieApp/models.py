
# Create your models here.
import uuid
from .validators import validate_zip
from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    pass

class UserDetails(models.Model):
    first_name = models.CharField(max_length=10, null=False)
    last_name = models.CharField(max_length=10, null=False)
    age = models.PositiveIntegerField(null=False)
    zip_code = models.CharField(max_length=5, validators=[validate_zip], null=False)
    gender = models.CharField(max_length=5, null=False)
    pets = models.CharField(max_length=3, null=False)
    housing_type = models.CharField(max_length=11, null=False)
    smoke = models.CharField(max_length=3, null=False)
    price = models.PositiveIntegerField(blank=False, null=False)
    pic = models.ImageField(default='default.png', blank=True, null=True)
    bio = models.CharField(max_length=100, null=False)
    user = models.OneToOneField(CustomUser, max_length=10, on_delete=models.CASCADE)

    def __str__(self):
        return "User ID: " + str(self.user)
        class Meta:
            order_with_respect_to = 'first_name'

class Match(models.Model):
    matchUser = models.CharField(max_length=50)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)


class Contact (models.Model):
    name = models.CharField(max_length=20, null=False)
    email = models.CharField(max_length=25, null=False)
    msg = models.CharField(max_length=100, null=False)
