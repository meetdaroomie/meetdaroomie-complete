from django.contrib import admin

# Register your models here.
from django.contrib import admin

# Register your models here.
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from .userForms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser, UserDetails, Match, Contact

class CustomUserAdmin(UserAdmin):
    model = CustomUser
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm

admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(UserDetails)
admin.site.register(Match)
admin.site.register(Contact)
