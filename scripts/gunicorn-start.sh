#!/bin/bash
NAME="Meetdaroomie"
DIR=/home/django/meetdaroomie-complete/MeetDaRoommieProject
USER=django
GROUP=django
WORKERS=3
BIND=unix:/home/django/run/gunicorn.sock
DJANGO_SETTINGS_MODULE=MeetdaRoommieProject.settings
DJANGO_WSGI_MODULE=MeetdaRoommieProject.wsgi
LOG_LEVEL=error
cd $DIR
source ../../venv/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DIR:$PYTHONPATH
exec ../../venv/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $WORKERS \
  --user=$USER \
  --group=$GROUP \
  --bind=$BIND \
  --log-level=$LOG_LEVEL \
  --log-file=-

